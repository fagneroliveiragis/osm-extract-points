from decouple import config
import psycopg2


def criar_tabela_malha(esquema='public', nome='municipios'):
    query = f'''
    drop table if exists {esquema}.{nome} cascade ;
    create table {esquema}.{nome} (
        id_pk serial primary key,
        ibge_code   varchar(40),
        the_geom geometry(MultiPolygon, 4326)
    );    
    '''
    with psycopg2.connect(config('DB')) as conn:
        with conn.cursor() as cur:  
            cur.execute(query)

def criar_tabela_dados_municipais(esquema='public', nome='dados_municipais'):
    query = f'''
        drop table if exists {esquema}.{nome} cascade ;
        create table {esquema}.{nome}(
            id_pk serial primary key,
            uf_id int4,
            uf_nome varchar(255),
            uf_sigla varchar(255),
            mesorregiao_id int4,
            mesorregiao_nome varchar(255),
            microrregiao_id int4,
            microrregiao_nome varchar(255),
            municipio_id varchar(255),
            municipio_nome varchar(255),
            regiao_id int4,
            regiao_imediata_id int4,
            regiao_imediata_nome varchar(255),
            regiao_intermediaria_id int4,
            regiao_intermediaria_nome varchar(255),
            regiao_nome varchar(255),
            regiao_sigla varchar(255)
    );
   '''
    with psycopg2.connect(config('DB')) as conn:
        with conn.cursor() as cur:  
            cur.execute(query)

def criar_view_municipios(esquema='public', nome='vw_municipios'):
    query = f'''
    create or replace view {esquema}.{nome} as 
    select 
        malha.id_pk,
        dados.uf_id,
        dados.uf_nome,
        dados.uf_sigla,
        dados.mesorregiao_id,
        dados.mesorregiao_nome,
        dados.microrregiao_id,
        dados.microrregiao_nome,
        dados.municipio_id,
        dados.municipio_nome,
        dados.regiao_id,
        dados.regiao_imediata_id,
        dados.regiao_imediata_nome,
        dados.regiao_intermediaria_id,
        dados.regiao_intermediaria_nome,
        dados.regiao_nome,
        dados.regiao_sigla,
        malha.the_geom
        from 
            public.dados_municipais dados join public.municipios malha
        on malha.ibge_code = dados.municipio_id;
    '''
    with psycopg2.connect(config('DB')) as conn:
        with conn.cursor() as cur:  
            cur.execute(query)

def criar_tabela_de_pontos(esquema='public', nome='pontos'):
    query = f'''
        drop table if exists {esquema}.{nome};
        create table public.pontos(
            id_pk serial primary key,
            osm_id character varying not null,
            categoria character varying not null,
            nome character varying,
            valor int4 DEFAULT 1,
            the_geom geometry(Point, 4326) not null
        );
    '''
    with psycopg2.connect(config('DB')) as conn:
        with conn.cursor() as cur:  
            cur.execute(query)

def run():
    criar_tabela_malha()
    criar_tabela_dados_municipais()
    criar_view_municipios()
    criar_tabela_de_pontos()

if __name__ == '__main__':
    run()