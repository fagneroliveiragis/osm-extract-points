import psycopg2
import requests
from decouple import config


def obter_bbox(uf_sigla, municipio_nome, table_name='public.vw_municipios'):
    query = f"""
        select '[' || 
			st_xmin(the_geom)::text  || ', ' ||
			st_ymin(the_geom)::text  || ', ' ||
			st_xmax(the_geom)::text  || ', ' ||
			st_ymax(the_geom)::text  ||
		']' as bbox from {table_name} 
        where "municipio_nome" = '{municipio_nome}' and uf_sigla = '{uf_sigla}';
    """

    with psycopg2.connect(config('DB')) as conn:
        with conn.cursor() as cur:
            cur.execute(query)
            bbox = str(cur.fetchone()[0])
    return bbox


def obter_dados(uf_sigla, municipio_nome):
    url = ''.join(
        [
            config('OVERPASS_URL'),
            config('OVERPASS_DIR'),
            obter_bbox(uf_sigla, municipio_nome).replace(
                '[',
                '[bbox='
            )
        ]
    )
    #filename = f'osm_data/{sigla_uf}_{nm_municipio}_output.osm'
    filename = 'osm_data/datasource.osm'
    data = requests.get(url)
    with open(filename, 'wb') as file:
        file.write(data.content)


def habilitar_postgres_hstore():
    query = '''
        CREATE EXTENSION IF NOT EXISTS hstore SCHEMA public;
    '''
    with psycopg2.connect(config('DB')) as conn:
        with conn.cursor() as cur:
            cur.execute(query)


def habilitar_postgres_ogr_fdw():
    query = '''
        CREATE EXTENSION IF NOT EXISTS ogr_fdw SCHEMA public;        
    '''
    with psycopg2.connect(config('DB')) as conn:
        with conn.cursor() as cur:
            cur.execute(query)

def apagar_OSM_FDW_server():
    query = f"""
        DROP SERVER IF EXISTS svr_osm cascade;
        """
    with psycopg2.connect(config('DB')) as conn:
        with conn.cursor() as cur:
            cur.execute(query)

def criar_OSM_FDW_server():
    apagar_OSM_FDW_server()
    query = f"""
            CREATE SERVER IF NOT EXISTS svr_osm 
            FOREIGN DATA WRAPPER ogr_fdw
            OPTIONS (
                datasource '{config('DATASOURCE')}',
                format 'OSM'
            );
            """

    with psycopg2.connect(config('DB')) as conn:
        with conn.cursor() as cur:
            cur.execute(query)

def importar_esquema_osm():
    query = f"""
            IMPORT FOREIGN SCHEMA ogr_all FROM SERVER svr_osm INTO public;
            """
    with psycopg2.connect(config('DB')) as conn:  
        with conn.cursor() as cur:
            cur.execute(query)


def teste():
    habilitar_postgres_hstore()
    habilitar_postgres_ogr_fdw()
    criar_OSM_FDW_server()
    importar_esquema_osm()


if __name__ == '__main__':
    teste()
