import get_data
import ibge_data_downloader
import prepare_database

if __name__ == '__main__':
    prepare_database.run()
    ibge_data_downloader.run()
