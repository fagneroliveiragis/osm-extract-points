from requests import get
from decouple import config
import psycopg2
import json


def cria_insert_malhas_municipais(attribute, geometry):
    prepared_geometry = f'''st_multi(st_transform(st_setsrid(st_geomfromgeojson('{geometry}'),4674),4326))'''
    values = f''' '{attribute}',{prepared_geometry}'''

    query = f'''insert into public.municipios(ibge_code, the_geom) values ({values})'''
    return query


def salva_geometrias_no_banco(features):
    with psycopg2.connect(config('DB')) as conn:
        with conn.cursor() as cur:
            for feature in features:
                ibge_code = feature['properties']['codarea']
                geometry = json.dumps(feature['geometry'])
                query = cria_insert_malhas_municipais(ibge_code, geometry)
                cur.execute(query)


def obter_malhas_municipais():
    url = ''.join([
        'https://servicodados.ibge.gov.br/api/v3/malhas/paises/BR',
        '?formato=application/vnd.geo+json&qualidade=minima&intrarregiao=municipio'
    ])

    header = {'Accept': 'application/vnd.geo+json'}
    response = get(url, headers=header)
    features = response.json()['features']

    return features


def criar_insert_dados_municipais(dado):
    ibge = dado.keys()
    pg_keys = [key.lower().replace('-', '_') for key in ibge]

    columns = ','.join(pg_keys)
    values = []

    for value in dado.values():
        if type(value) == type(''):
            value = value.replace("'", "''")
            value = "'" + value + "'"
        values += [str(value)]
    part = "(" + ', '.join(values) + ")"
    query = f'''insert into public.dados_municipais ({columns}) values {part}'''
    return query


def salva_dados_municipais(dados):
    with psycopg2.connect(config('DB')) as conn:
        with conn.cursor() as cur:
            for dado in dados:
                query = criar_insert_dados_municipais(dado)
                cur.execute(query)


def obter_dados_municipais():
    url = ''.join([
        'https://servicodados.ibge.gov.br/api/v1/localidades/municipios',
        '?view=nivelado'
    ])
    header = {'Accept': 'application/vnd.geo+json'}
    response = get(url, headers=header)
    municipios = response.json()
    return municipios

def run():
    municipios = obter_malhas_municipais()
    salva_geometrias_no_banco(municipios)
    dados = obter_dados_municipais()
    salva_dados_municipais(dados)

if __name__ == '__main__':
    run()
